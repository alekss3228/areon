import React, {useEffect, useState} from "react"
import "./App.scss"
import {CardList} from "./components/CardList/CardList";
import {Header} from "./components/Header/Header";



export function App () {


    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || [])
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || [])

    useEffect(() => {
        localStorage.setItem('basket', JSON.stringify(basket))
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [basket, favorites])

    function basketUser(item) {
        let repetition = false
        basket.forEach(el => {
            if (el.article === item.article) {
                repetition = true
                el.quantity++
                setBasket([...basket])
            }
        })
        if (!repetition) {
            setBasket([...basket, item])
        }
    }
    function delBasket (item) {
        basket.forEach(el => {
            if (el.article === item.article) {
                setBasket(basket.filter(el => el.article !== item.article))
            }
        })
    }
    function favoritesUser(item) {
        let repetition = false
        favorites.forEach(el => {
            if (el.article === item.article) {
                repetition = true
                setFavorites(favorites.filter(el => el.article !== item.article))
            }
        })
        if (!repetition) {
            setFavorites([...favorites, item])
        }
    }



    return (
    <div className="App">

        <Header basket={basket} favorites={favorites} delFav={favoritesUser} delBasket={delBasket}/>

        <CardList basket={basketUser} favorites={favoritesUser} favList={favorites}/>

    </div>
  )
}


