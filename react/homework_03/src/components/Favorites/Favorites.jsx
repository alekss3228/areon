import React, {useState} from 'react';
import "./Favorites.scss"
import {NavMenuItem} from "../NavMenuItem/NavMenuItem";


export function Favorites () {

    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || [])

    function delItem (item) {
        favorites.forEach(el => {
            if (el.article === item.article) {
                setFavorites(favorites.filter(el => el.article !== item.article))
                localStorage.setItem('favorites', JSON.stringify(favorites.filter(el => el.article !== item.article)))
            }
        })
    }

    return (
        <section className="favorites">
            <h2 className="favorites__title">Favorites</h2>
            <div className="favorites__list">
                {favorites.map((nut) => (
                    <NavMenuItem key = {nut.article} nut = {nut} del={delItem} />
                ))}
            </div>
        </section>
    )
}

