import React, {useState} from 'react';
import "./NavbarModal.scss"
import PropTypes from "prop-types";
import {Modal} from "../Modal/Modal";
import {Button} from "../Button/Button";

export function NavbarModal (props) {

    const [modal, setModal] = useState(false)


        return (
            <div className="Navbar-item">
                <img className="Navbar-item__img" src={props.item.url} alt=""/>
                <div>
                <h3 className="Navbar-item__title">{props.item.name}</h3>
                <p className="Navbar-item__text"><strong>{props.item.price}</strong> грн</p>
                </div>
                {props.basket &&
                    <p><strong>{props.item.quantity}</strong> шт.</p>
                }
                <p className="Navbar-item__del" onClick={() => setModal(true)}>X</p>
                {modal &&
                    <Modal
                        header = "Ви впевнені, що хочете видалити цей товар?"
                        text = "Товар буде видаленно."
                        closeButton = {() => setModal(false)}
                        actions = {<>
                            <Button backgroundColor = "darkorange"
                                    text = "Ok"
                                    ButtonClick = {() => {
                                        if (props.favorites) {
                                            props.delFav(props.item)
                                        }
                                        if (props.basket) {
                                            props.delBasket(props.item)
                                        }
                                        setModal(false)

                                    }}/>
                            <Button backgroundColor = "darkorange"
                                    text = "Cancel"
                                    ButtonClick = {() => setModal(false)}/>
                        </>}
                    />}
            </div>
        )


}
NavbarModal.propTypes = {
    url: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    item: PropTypes.object,
    quantity: PropTypes.number,
}
NavbarModal.defaultProps = {
    name: "nut",
    url: "./img/Peanut.jpg",
    price: 0,
}
