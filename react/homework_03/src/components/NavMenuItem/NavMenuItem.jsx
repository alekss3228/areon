import React, {useState} from 'react';
import {Modal} from "../Modal/Modal";
import {Button} from "../Button/Button";
import "./NavMenuItem.scss"

export function NavMenuItem (props) {

    const [modal, setModal] = useState(false)

    return (
        <div className="list__items" key={props.nut.article}>
            <img className="list__items__img" src={props.nut.url} alt=""/>
            <h3 className="list__items__title">{props.nut.name}</h3>
            <p><strong>{props.nut.quantity}</strong> шт.</p>
            <p className="list__items__text">Вартість <strong>{props.nut.price * props.nut.quantity}</strong> грн</p>
            <p className="list__items__del" onClick={() => setModal(true)}>X</p>
            {modal &&
                <Modal
                    header = "Ви впевнені, що хочете видалити цей товар?"
                    text = "Товар буде видаленно."
                    closeButton = {() => setModal(false)}
                    actions = {<>
                        <Button backgroundColor = "darkorange"
                                text = "Ok"
                                ButtonClick = {() => {
                                    props.del(props.nut)
                                    setModal(false)

                                }}/>
                        <Button backgroundColor = "darkorange"
                                text = "Cancel"
                                ButtonClick = {() => setModal(false)}/>
                    </>}
                />
            }
        </div>
    )
}