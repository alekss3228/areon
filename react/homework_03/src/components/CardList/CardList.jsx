import React, {useEffect, useState} from 'react'
import {Card} from "../Card/Card";
import "./CardList.scss"
import PropTypes from "prop-types";


export function CardList (props) {

    const [nuts, setNuts] = useState([])

    useEffect(() => {
        fetch(`./data.json`)
                .then(r => r.json())
                .then(data => {
                    setNuts( data)
                })
    }, [])

        return (
            <section className="Product">
            <h2 className="Product__title">Nuts</h2>
             <div className="Product__list">
                 {nuts.map((nut) => (
                     <Card key = {nut.article} nut = {nut} basket={props.basket} favorites={props.favorites} favList={props.favList}/>
                     ))}
             </div>
            </section>
        )
}
CardList.propTypes = {
    basket: PropTypes.func,
    favorites: PropTypes.func,
    del: PropTypes.func,
}