import React, {useEffect, useState} from 'react'
import "./Card.scss"
import {Button} from "../Button/Button";
import {Modal} from "../Modal/Modal";
import {Star} from "../SVG/star";
import PropTypes from "prop-types";





export function Card (props) {

    const [favoritesColor, setFavoritesColor] = useState("black")
    const [modal, setModal] = useState(false)


    useEffect(() => {
        setFavoritesColor("black")
        props.favList.forEach(el => {
                    if (el.article === props.nut.article) {
                        setFavoritesColor("orange")
                    }
                })
    }, [props])


        return (
            <a className="Link" href="#" onClick={event => event.preventDefault()}>
                <img className="Link__img" src={props.nut.url} alt=""/>
                <h3 className="Link__title">{props.nut.name}</h3>
                <p className="Link__text"><strong>{props.nut.price}</strong> грн</p>
                <Star color = {favoritesColor} size = "20px" clickStar = {
                    () => {
                        props.favorites(props.nut)
                        favoritesColor === "black" ? setFavoritesColor("orange") : setFavoritesColor("black")

                    }
                }/>
                <Button backgroundColor = "black" text = "Купить" ButtonClick = {() => setModal(true)}/>
                {modal &&
                    <Modal header = "Ви впевнені, що хочете додати цей товар у свій кошик?"
                           text = "Щоб переглянути кошик, натисніть на його іконку, в шапці сайту."
                           closeButton = {() => setModal(false)}
                           actions = {<>
                               <Button backgroundColor = "darkorange"
                                       text = "Ok"
                                       ButtonClick = {() => {
                                           props.basket(props.nut)
                                           setModal(false)

                                       }}/>
                               <Button backgroundColor = "darkorange"
                                       text = "Cancel"
                                       ButtonClick = {() => setModal(false)}/>
                           </>}
                    />}
            </a>
        )

}
Card.propTypes = {
    name: PropTypes.string,
    picture: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.number,
    nut: PropTypes.object,
    basket: PropTypes.func,
    favorites: PropTypes.func,
    del: PropTypes.func,
}
Card.defaultProps = {
    name: "nut",
    picture: "./img/Peanut.jpg",
    price: 0,
    article: 1,
}