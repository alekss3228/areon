import React from 'react';
import {Link, Outlet} from "react-router-dom";
import "./Nav_menu.scss"
import {ButtonBack} from "../Button_back/Button_back";

export function NavMenu () {
    return (
        <>
        <menu className="menu">
            <Link className="menu__item" to="/">Home</Link>
            <Link className="menu__item" to="basket">Basket</Link>
            <Link className="menu__item" to="favorites">Favorites</Link>
            <ButtonBack/>
        </menu>
        <Outlet/>
        </>
    )
}