import React from "react"
import "./Button.scss"
import PropTypes from "prop-types";

export class Button extends React.Component {
constructor(props) {
    super(props)
}


    render() {
        return (
            <button className={"button"} style={{backgroundColor: this.props.backgroundColor}} onClick={(ev) => {
                ev.preventDefault()
                this.props.ButtonClick()
            }}>{this.props.text}</button>
        )
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    ButtonClick: PropTypes.func,
}
Button.defaultProps = {
    backgroundColor: "white",
    text: "ok",
}