import React from 'react'
import "./Card.scss"
import {Button} from "../Button/Button";
import {Modal} from "../Modal/Modal";
import {Star} from "../SVG/star";
import PropTypes, {object} from "prop-types";





export class Card extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            favoritesColor: "black",
            modal: false,
            favorites: JSON.parse(localStorage.getItem('favorites')) || [],
        }

    }
    componentDidMount() {
        this.state.favorites.forEach(el => {
            if (el.article === this.props.nut.article)
                this.setState({favoritesColor: "orange"})
        })
    }

    render() {
        return (
            <a className="Linc" href="#">
                <img className="Linc__img" src={this.props.picture} alt=""/>
                <h3 className="Linc__title">{this.props.name}</h3>
                <p className="Linc__text"><strong>{this.props.price}</strong> грн</p>
                <Star color = {this.state.favoritesColor} size = "20px" clickStar = {
                    () => {
                            this.setState({favoritesColor: "orange"})
                            this.props.favorites(this.props.nut)
                    }
                }/>
                <Button backgroundColor = "black" text = "Купить" ButtonClick = {() => this.setState({modal: true})}/>
                {this.state.modal &&
                    <Modal header = "Ви впевнені, що хочете додати цей товар у свій кошик?"
                           text = "Щоб переглянути кошик, натисніть на його іконку, в шапці сайту."
                           closeButton = {() => this.setState({modal: false})}
                           actions = {<>
                               <Button backgroundColor = "darkorange"
                                       text = "Ok"
                                       ButtonClick = {() => {
                                           this.props.basket(this.props.nut)
                                           this.setState({modal: false})

                                       }}/>
                               <Button backgroundColor = "darkorange"
                                       text = "Cancel"
                                       ButtonClick = {() => this.setState({modal: false})}/>
                           </>}
                    />}
            </a>
        )
    }

}
Card.propTypes = {
    name: PropTypes.string,
    picture: PropTypes.string,
    price: PropTypes.number,
    article: PropTypes.number,
    nut: PropTypes.object,
    basket: PropTypes.func,
    favorites: PropTypes.func,
}
Card.defaultProps = {
    name: "nut",
    picture: "./img/Peanut.jpg",
    price: 0,
    article: 1,
}