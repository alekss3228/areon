import React from 'react'
import {Card} from "../Card/Card";
import "./CardList.scss"
import PropTypes from "prop-types";



export class CardList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            nuts: []
        }
    }
    componentDidMount() {
        fetch(`./data.json`)
            .then(r => r.json())
            .then(data => {
                this.setState({nuts: data})
            })
    }

    render() {
        return (
            <section className="Product">
            <h2 className="Product__title">Nuts</h2>
             <div className="Product__list">
                 {this.state.nuts.map((nut) => (
                     <Card key = {nut.article} name = {nut.name} price = {nut.price} picture = {nut.url} nut = {nut} basket={this.props.basket} favorites={this.props.favorites}/>
                     ))}
             </div>
            </section>
        )
    }
}
CardList.propTypes = {
    basket: PropTypes.func,
    favorites: PropTypes.func,
}