import React from "react";
import {Basket} from "../SVG/basket";
import {Star} from "../SVG/star";
import "./Header.scss"
import {Button} from "../Button/Button";
import {NavbarModal} from "../NavbarModal/NavbarModal";
import PropTypes from "prop-types";

export class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            basketItem: false,
            favoritesItem: false,
        }
    }

    render() {
        return (
            <header className="header">
                <h2 className="header__title">Магазин горіхів</h2>
                <div className="header__items">
                    <figure className="header__items__fig">
                        <Basket size = "40px" clickBasket = {() => {
                            this.setState({basketItem: true})
                            this.setState({favoritesItem: false})
                        }}/>
                        <span className="header__items__counter">{this.props.basket.length}</span>
                    </figure>
                    <figure className="header__items__fig">
                        <Star color = "orange" size = "40px" clickStar = {() => {
                            this.setState({favoritesItem: true})
                            this.setState({basketItem: false})
                        }}/>
                        <span className="header__items__counter">{this.props.favorites.length}</span>
                    </figure>
                </div>
                {this.state.basketItem &&
                    <div className="Navbar-modal">
                        <p className="Navbar-modal__button" onClick={() => this.setState({basketItem: false})}>X</p>
                        {this.props.basket.length > 0 ?
                            this.props.basket.map((item) => (
                            <NavbarModal key={item.article} item={item}/>
                            )) : (<p className="Navbar-modal__text">Товарів немає</p>)}
                        <Button backgroundColor = "#777777"
                                text = "Cancel"
                                ButtonClick = {() => this.setState({basketItem: false})}/>
                    </div>
                }
                {this.state.favoritesItem &&
                    <div className="Navbar-modal">
                        <p className="Navbar-modal__button" onClick={() => this.setState({favoritesItem: false})}>X</p>
                        {this.props.favorites.length > 0 ?
                            this.props.favorites.map((item) => (
                                <NavbarModal key={item.article} item={item}/>
                            )) : (<p className="Navbar-modal__text">Немає обраних</p>)}
                        <Button backgroundColor = "#777777"
                                text = "Cancel"
                                ButtonClick = {() => this.setState({favoritesItem: false})}/>
                    </div>
                }
            </header>
        )
    }
}
Header.propTypes = {
    basket: PropTypes.array,
    favorites: PropTypes.array,
}
Header.defaultProps = {
    basket: [],
    favorites: [],
}