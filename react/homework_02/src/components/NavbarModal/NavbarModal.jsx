import React from 'react';
import "./NavbarModal.scss"
import PropTypes from "prop-types";

export class NavbarModal extends React.Component{

    render() {
        return (
            <div className="Navbar-item">
                <img className="Navbar-item__img" src={this.props.item.url} alt=""/>
                <div>
                <h3 className="Navbar-item__title">{this.props.item.name}</h3>
                <p className="Navbar-item__text"><strong>{this.props.item.price}</strong> грн</p>
                </div>
            </div>
        )
    }

}
NavbarModal.propTypes = {
    url: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    item: PropTypes.object,
}
NavbarModal.defaultProps = {
    name: "nut",
    url: "./img/Peanut.jpg",
    price: 0,
}
