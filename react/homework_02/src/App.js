import React from "react"
import "./App.scss"
import {CardList} from "./components/CardList/CardList";
import {Header} from "./components/Header/Header";


export class App extends React.Component {
constructor(props) {
    super(props)
    this.state = {
        basket: JSON.parse(localStorage.getItem('basket')) || [],
        favorites: JSON.parse(localStorage.getItem('favorites')) || [],
    }
    this.basketUser = this.basketUser.bind(this)
    this.favoritesUser = this.favoritesUser.bind(this)
}
    basketUser(item) {
    // let repetition = false
    //     this.state.basket.forEach(el => {
    //         if (el.article === item.article)
    //             repetition = true
    //     })
    //
    //     if (!repetition)
            this.setState({basket: [...this.state.basket, item]},
                () => localStorage.setItem('basket', JSON.stringify(this.state.basket)))
    }
    favoritesUser(item) {
        let repetition = false
        this.state.favorites.forEach(el => {
            if (el.article === item.article)
                repetition = true
        })

        if (!repetition)
        this.setState({favorites: [...this.state.favorites, item]},
            () => localStorage.setItem('favorites', JSON.stringify(this.state.favorites)))
    }
    render() {

    return (
    <div className="App">

        <Header basket={this.state.basket} favorites={this.state.favorites}/>

        <CardList basket={this.basketUser} favorites={this.favoritesUser} />

    </div>
  )}
}


