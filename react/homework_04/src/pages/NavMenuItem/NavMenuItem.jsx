import React, {useState} from 'react';
import {Modal} from "../../components/Modal/Modal";
import {Button} from "../../components/Button/Button";
import "./NavMenuItem.scss"
import {useDispatch} from "react-redux";

export function NavMenuItem (props) {

    const [modal, setModal] = useState(false)

    const dispatch = useDispatch()

    return (
        <div className="list__items" key={props.nut.article}>
            <img className="list__items__img" src={props.nut.url} alt=""/>
            <h3 className="list__items__title">{props.nut.name}</h3>
            <p><strong>{props.nut.quantity}</strong> шт.</p>
            <p className="list__items__text">Вартість <strong>{props.nut.price * props.nut.quantity}</strong> грн</p>
            <p className="list__items__del" onClick={() => {
                setModal(true)
                dispatch({type: 'OPEN_MODAL'})
            }}>X</p>
            {modal &&
                <Modal
                    header = "Ви впевнені, що хочете видалити цей товар?"
                    text = "Товар буде видаленно."
                    closeButton = {() => {
                        setModal(false)
                        dispatch({type: 'CLOSED_MODAL'})
                    }}
                    actions = {<>
                        <Button backgroundColor = "darkorange"
                                text = "Ok"
                                ButtonClick = {() => {
                                    props.del(props.nut)
                                    setModal(false)
                                    dispatch({type: 'CLOSED_MODAL'})
                                }}/>
                        <Button backgroundColor = "darkorange"
                                text = "Cancel"
                                ButtonClick = {() => {
                                    setModal(false)
                                    dispatch({type: 'CLOSED_MODAL'})
                                }}/>
                    </>}
                />
            }
        </div>
    )
}