import React, {useState} from 'react';
import "./Basket.scss"
import {NavMenuItem} from "../NavMenuItem/NavMenuItem";

export function Basket () {

    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || [])

    function delItem (item) {
        basket.forEach(el => {
            if (el.article === item.article) {
                setBasket(basket.filter(el => el.article !== item.article))
                localStorage.setItem('basket', JSON.stringify(basket.filter(el => el.article !== item.article)))
            }
        })
    }

    return (
        <section className="basket">
            <h2 className="basket__title">Basket</h2>
            <div className="basket__list">
                {basket.map((nut) => (
                   <NavMenuItem key = {nut.article} nut = {nut} del={delItem} />
                ))}
            </div>
        </section>
    )
}

