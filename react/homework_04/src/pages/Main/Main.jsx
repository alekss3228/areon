import React, {useEffect} from 'react';
import {Header} from "../../components/Header/Header";
import {CardList} from "../../components/CardList/CardList";
import "./Main.scss"
import {useDispatch, useSelector} from "react-redux";

export function Main () {

    const favorites = useSelector(state => state.favorites)
    const basket = useSelector(state => state.basket)
    const dispatch = useDispatch()

    useEffect(() => {
        localStorage.setItem('basket', JSON.stringify(basket))
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [basket, favorites])


    function delBasket (item) {
        basket.forEach(el => {
            if (el.article === item.article) {
                dispatch({type: 'REMOVE_PRODUCT_FROM_BASKET_ACTION_TYPE', payload: { item }})
            }
        })
    }
    function favoritesUser(item) {
        let repetition = false
        favorites.forEach(el => {
            if (el.article === item.article) {
                repetition = true
                dispatch({type: 'REMOVE_PRODUCT_FROM_FAVORITES_ACTION_TYPE', payload: { item }})
            }
        })
        if (!repetition) {
            dispatch({type: 'ADD_PRODUCT_TO_FAVORITES_ACTION_TYPE', payload: { item }})
        }
    }

    return (
        <div className="Main">

            <Header basket={basket} favorites={favorites} delFav={favoritesUser} delBasket={delBasket}/>

            <CardList favorites={favoritesUser} favList={favorites}/>

        </div>
    )
}