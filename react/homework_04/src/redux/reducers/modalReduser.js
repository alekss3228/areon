

export function modalReducer (state = {isModal: false}, action) {
    switch (action.type) {
        case 'OPEN_MODAL':
            return {...state, isModal: true}
        case 'CLOSED_MODAL':
            return {...state, isModal: false}
        case 'OPEN_BASKET_MODAL':
            return {...state, productId: action.payload.productId, isBasketModal: true}
        case 'CLOSE_BASKET_MODAL':
            return {...state, productId: null, isBasketModal: false}
        default:
            return state
    }
}