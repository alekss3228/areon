import React from 'react';
import {Link, Outlet} from "react-router-dom";
import "./NavMenu.scss"
import {ButtonBack} from "../../pages/Button_back/Button_back";
import {Modal} from "../Modal/Modal";
import {Button} from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";

function addProductToBasketThunk () {
    return (dispatch, getState) => {
        const state = getState()
        const productId = state.modal.productId
        const basket = state.basket
        let repetition = false
        const product = state.products.products.find(product => product.article === productId)
        basket.forEach(el => {
            if (el.article === productId) {
                repetition = true
                dispatch({type: 'RE-ADD_ITEM_TO_CART_TYPE_OF_ACTION', payload: { productId }})
            }
        })
        if (!repetition) {
            dispatch({type: 'ADD_PRODUCT_TO_BASKET_ACTION_TYPE', payload: { item: product }})
        }
        dispatch({type: 'CLOSE_BASKET_MODAL'})
    }
}

// function basketUser(item) {
//
// }
export function NavMenu () {

    const isBasketModalOpen = useSelector(state => state.modal.isBasketModal)
    const dispatch = useDispatch()

    return (
        <>
        <menu className="menu">
            <Link className="menu__item" to="/">Home</Link>
            <Link className="menu__item" to="basket">Basket</Link>
            <Link className="menu__item" to="favorites">Favorites</Link>
            <ButtonBack/>
        </menu>
            {isBasketModalOpen &&
                <Modal header = "Ви впевнені, що хочете додати цей товар у свій кошик?"
                       text = "Щоб переглянути кошик, натисніть на його іконку, в шапці сайту."
                       closeButton = {() => {
                           // setModal(false)
                           dispatch({type: 'CLOSE_BASKET_MODAL'})
                       }}
                       actions = {<>
                           <Button backgroundColor = "darkorange"
                                   text = "Ok"
                                   ButtonClick = {() => {
                                       // props.basket(props.nut)
                                       // setModal(false)
                                       dispatch(addProductToBasketThunk())
                                   }}/>
                           <Button backgroundColor = "darkorange"
                                   text = "Cancel"
                                   ButtonClick = {() => {
                                       // setModal(false)
                                       dispatch({type: 'CLOSE_BASKET_MODAL'})
                                   }}/>
                       </>}
                />}
        <Outlet/>
        </>
    )
}