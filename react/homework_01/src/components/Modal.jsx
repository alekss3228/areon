import React from 'react';
import "../styles/Modal.scss"
import {Button} from "./Button";

export class Modal extends React.Component {
constructor(props) {
    super(props);
}
    render() {
    return (
        <div className="modal" onClick={(ev) => this.props.closeButton()}>
            <div className="modal__content" onClick={(ev) => ev.stopPropagation()}>
                <div className="modal__content__title">
                <h1 className="modal__content__title__text">{this.props.header}</h1>
                <span className="modal__content__title__close" onClick={() => this.props.closeButton()}>X</span>
                </div>
                <p className="modal__content__text">{this.props.text}</p>
                <div className="modal__button">
                    {this.props.actions}
                </div>
            </div>
        </div>
    )
    };
};

