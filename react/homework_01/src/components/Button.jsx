import React from "react";
import '../styles/Button.scss'

export class Button extends React.Component {
constructor(props) {
    super(props);
}


    render() {
        return (
            <button className={"button"} style={{backgroundColor: this.props.backgroundColor}} onClick={() => this.props.ButtonClick()}>{this.props.text}</button>
        )
    }
}