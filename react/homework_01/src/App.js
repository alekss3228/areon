import React from "react";
import {Button} from "./components/Button";
import './styles/App.scss'
import {Modal} from "./components/Modal";


export class App extends React.Component {
constructor(props) {
    super(props);
    this.state = {
        ModalFirst: false,
        ModalSecond: false
    }
}
    render() {

    return (
    <div className="App">
      <Button backgroundColor = "#89CA62" text = "Open first modal" ButtonClick = {() => this.setState({ModalFirst: true})}/>
      <Button backgroundColor = "#14B9D5" text = "Open second modal" ButtonClick = {() => this.setState({ModalSecond: true})}/>
        {this.state.ModalFirst &&
            <Modal header = "Do you want to delete this file?"
                   text = "Once deleted, the file cannot be recovered."
                   closeButton = {() => this.setState({ModalFirst: false})}
                   actions = {<>
                       <Button backgroundColor = "darkorange"
                               text = "Ok"
                               ButtonClick = {() => this.setState({ModalFirst: false})}/>
                       <Button backgroundColor = "darkorange"
                               text = "Cancel"
                               ButtonClick = {() => this.setState({ModalFirst: false})}/></>}
            />}
        {this.state.ModalSecond &&
            <Modal header = "Do you want to save this file?"
                   text = "After saving, you will receive a confirmation email."
                   closeButton = {() => this.setState({ModalSecond: false})}
                   actions = {<>
                       <Button backgroundColor = "darkorange"
                               text = "Ok"
                               ButtonClick = {() => this.setState({ModalSecond: false})}/>
                       <Button backgroundColor = "darkorange"
                               text = "Cancel"
                               ButtonClick = {() => this.setState({ModalSecond: false})}/></>}
            />}
    </div>
  )};
}


